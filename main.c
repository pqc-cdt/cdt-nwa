#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pcap.h>
#include "device_manager.h"
#include "report_manager.h"
#include "report_writer.h"
#include "utils.h"

device_manager *manager;

void handle_sig(int signum){
    if(signum==SIGINT||signum==SIGQUIT||signum==SIGTERM){
        device_manager_stop_capture(manager);
    }
} 


int main() {
    manager=NULL;
    signal(SIGHUP, handle_sig);
    signal(SIGINT, handle_sig);
    signal(SIGQUIT, handle_sig);
    signal(SIGTERM, handle_sig);

    report_writer *rpw=report_writer_init();
    report_writer_open_file(rpw, "report.nwreport");

    report_manager *rpm=reportmanager_init(rpw,0);

    manager = device_manager_init(rpm);
    device_manager_list_devices(manager);
    device_manager_start_capture(manager);


    device_manager_free(manager);
    
    reportmanager_deinit(rpm);

    report_writer_close_file(rpw);
    report_writer_deinit(rpw);

    printf("shutdown gracefully\n");
    return 0;
}