#include <stdlib.h>
#include <pcap.h>
#include "device_manager.h"
#include <string.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <netinet/tcp.h>
#include "analyzer.h"
#include "report_event.h"
#include "lsof_parser.h"
#include "report_manager.h"

device_manager *manager_glob=NULL;//TODO: manager unknown in handle_packet

device_manager *device_manager_init(report_manager *rpm) {

    // Initialize device manager
    device_manager *manager = calloc(1, sizeof(device_manager));
    if (!manager) { exit(1); }
    manager->devices = NULL;
    manager->device_count = 0;
    manager->handle=NULL;
    manager->rpm=rpm;
    manager_glob=manager;

    // Query all installed devices on the system
    if (pcap_findalldevs(&manager->devices, manager->dev_errbuf) == PCAP_ERROR) {
        strcpy(manager->dev_errbuf, "device_manager :: Failed to query network devices.");
        return manager;
    }
    // Count available devices
    for (pcap_if_t *device = manager->devices; device != NULL; device = device->next) {
        manager->device_count++;
    }
    if (manager->device_count == 0) {
        strcpy(manager->dev_errbuf, "device_manager :: No network devices found.");
    }

    return manager;
}


void device_manager_start_capture(device_manager *manager) {
    // TODO: For now, we capture on the first device (device0).
    // Later on, we would like to capture a provided selection of devices, or all devices.
    if (!manager) { exit(1); }

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_live(manager->devices[0].name, 65535, 1, 0, errbuf);
    if (handle == NULL) {
        printf("device_manager :: Failed to open device %s: %s\n", manager->devices[0].name, errbuf);
        exit(1);
    }
    manager->handle=handle;
    printf("device_manager :: Capturing on interface: %s\n", manager->devices[0].name);
    pcap_loop(handle, -1, device_manager_handle_packet, NULL);
    pcap_close(handle);
}

void device_manager_handle_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {

    struct ether_header *eth_header;
    eth_header = (struct ether_header *) packet;

    if (ntohs(eth_header->ether_type) == ETHERTYPE_IPV6){
        printf("IPV6-Package.\n");
        //TODO IPV6 support to be implemented.
    }

    // Ensure the packet is an IP packet
    if (ntohs(eth_header->ether_type) != ETHERTYPE_IP) {//TODO this only filters for ipv4. Later also ETHERTYPE_IPV6
        reportmanager_add_unidentified(manager_glob->rpm);
        return;
    }

    //
    struct ip *ip;
    struct tcphdr *tcp;
    ip = (struct ip *) (packet + sizeof(struct ether_header));
    tcp = (struct tcphdr *) (packet + sizeof(struct ether_header) + sizeof(struct ip));

    char *src = inet_ntoa(ip->ip_src);

    char *dst = inet_ntoa(ip->ip_dst);

    // Calculate the total packet length
    uint16_t total_length = ntohs(ip->ip_len);

    // Calculate the lengths of the headers
    uint16_t ethernet_header_length = sizeof(struct ether_header);
    uint16_t ip_header_length = ip->ip_hl * 4;
    uint16_t tcp_header_length = tcp->th_off * 4;

    // Calculate the TCP payload length
    uint16_t payload_length = total_length - (ethernet_header_length + ip_header_length + tcp_header_length);
        
        unsigned char* payload = packet + sizeof(struct ether_header) + ip_header_length + tcp_header_length;
        int is_tls_header_result=is_tls_header(payload, payload_length);
        if(is_tls_header_result>0){
            printf("payload_length: %d\n", payload_length);
            printf("Source: %s:%d \n", src, ntohs(tcp->source));
            printf("Destination: %s:%d \n", dst, ntohs(tcp->dest));
            printf("HTTPs traffic detected!\n");
            
            analyzer_result azr=analyze_package(payload, payload_length, payload_length);
            
            //add sock info
            report_event rp_ev;
            resolve_origin_process(ntohs(tcp->source), rp_ev.appname, 16);
            rp_ev.report_tls_findings=azr;
            rp_ev.pid=0;//todo
            rp_ev.gid=0;//todo
            rp_ev.package_direction=0;//todo
            rp_ev.report_socket.ip_ver=4;//alternatively the definitions ETHERTYPE_IP/6 or AF_INET/AF_INET6 could be used.
            rp_ev.report_socket.dst_ip.as_u32[0]=ip->ip_dst.s_addr;
            rp_ev.report_socket.src_ip.as_u32[0]=ip->ip_src.s_addr; 
            rp_ev.report_socket.dst_port=ntohs(tcp->dest);
            rp_ev.report_socket.src_port=ntohs(tcp->source);
            printf("V4 SRC IP: %d.%d.%d.%d, DST IP:  %d.%d.%d.%d \n", (int)(rp_ev.report_socket.src_ip.as_u8[0]), (int)(rp_ev.report_socket.src_ip.as_u8[1]), (int)(rp_ev.report_socket.src_ip.as_u8[2]), (int)(rp_ev.report_socket.src_ip.as_u8[3]), (int)(rp_ev.report_socket.dst_ip.as_u8[0]), (int)(rp_ev.report_socket.dst_ip.as_u8[1]), (int)(rp_ev.report_socket.dst_ip.as_u8[2]), (int)(rp_ev.report_socket.dst_ip.as_u8[3]) );
            printf("Source port: %d, dest port: %d \n", rp_ev.report_socket.src_port, rp_ev.report_socket.dst_port);
            reportmanager_add_entry(manager_glob->rpm, rp_ev);
        } else{
            reportmanager_add_unidentified(manager_glob->rpm);
        } 

}

void device_manager_stop_capture(device_manager *manager) {
        if(manager==NULL){
        printf("error: device manager handle is null\n");
        return;
    } 
    if(manager->handle==NULL){
        printf("error: device manager pcap handle is null\n");
        return;
    } 
   pcap_breakloop(manager->handle); 
}

void device_manager_list_devices(device_manager *manager) {
    for (pcap_if_t *device = manager->devices; device != NULL; device = device->next) {
        printf("Device Name: %s\n", device->name);
        printf("Device Description: %s\n", device->description);
        printf("\n");
    }
    printf("Found %i device(s)\n", manager->device_count);
}

void device_manager_free(device_manager *manager) {
    pcap_freealldevs(manager->devices);
    free(manager);
}
