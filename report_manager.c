#include <stdlib.h>
#include "report_manager.h"
#include "report_writer.h"
#include <time.h>
#include <string.h>

report_manager* reportmanager_init(report_writer* rpw, int write_after){//TODO: write after 
    report_manager* rpm;
    rpm= (report_manager*)calloc(1, sizeof(report_manager));
    rpm->rpw=rpw;
    rpm->write_after=write_after;
    rpm->total_packet_count=0;
    rpm->identified_packet_count=0;
    //report_writer_open_file("report.nwreport");
    report_writer_enter_object(rpm->rpw, NULL);
    time_t clk = time(NULL);
    char timebuffer[50];
     strcpy(timebuffer, ctime(&clk));
     timebuffer[strlen(timebuffer)-1]=0;
    report_writer_write_value_str(rpm->rpw, "start-timestamp",timebuffer);
    report_writer_enter_array(rpm->rpw, "traffic");
    return rpm;
}


void reportmanager_add_unidentified(report_manager *rpm) {
    rpm->total_packet_count++;
}


void reportmanager_add_entry(report_manager *rpm, report_event reportevent) {
    
    char ip_print_string[60];
    rpm->total_packet_count++;
     
    rpm->identified_packet_count++;
    report_writer_enter_object(rpm->rpw, NULL);
    report_writer_write_value_str(rpm->rpw, "protocol", "TLS");
    time_t clk = time(NULL);
    char timebuffer[50];
     strcpy(timebuffer, ctime(&clk));
     timebuffer[strlen(timebuffer)-1]=0;
    report_writer_write_value_str(rpm->rpw, "timestamp",timebuffer);

    report_writer_write_value_str(rpm->rpw, "process", reportevent.appname);
    if(reportevent.report_socket.ip_ver==4){
        sprintf(ip_print_string, "%d.%d.%d.%d",(int)(reportevent.report_socket.src_ip.as_u8[0]), (int)(reportevent.report_socket.src_ip.as_u8[1]), (int)(reportevent.report_socket.src_ip.as_u8[2]), (int)(reportevent.report_socket.src_ip.as_u8[3]));
    } else{
        sprintf(ip_print_string, "FIX-ME: IPV6 formating");
    } 
    
    report_writer_write_value_str(rpm->rpw, "source", ip_print_string);
    report_writer_write_value_int(rpm->rpw, "source-port", reportevent.report_socket.src_port);
    if(reportevent.report_socket.ip_ver==4){
        sprintf(ip_print_string, "%d.%d.%d.%d",(int)(reportevent.report_socket.dst_ip.as_u8[0]), (int)(reportevent.report_socket.dst_ip.as_u8[1]), (int)(reportevent.report_socket.dst_ip.as_u8[2]), (int)(reportevent.report_socket.dst_ip.as_u8[3]));
    } else{
        sprintf(ip_print_string, "FIX-ME: IPV6 formating");
    }
    report_writer_write_value_str(rpm->rpw, "destination",ip_print_string);
    report_writer_write_value_int(rpm->rpw, "dest-port", reportevent.report_socket.dst_port);
    report_writer_write_value_str(rpm->rpw, "tls-type", reportevent.report_tls_findings.tls_request_type==0x01?"Clienthello":"Serverhello");
    if(reportevent.report_tls_findings.tls_request_type==0x01)
    {
        report_writer_write_value_str(rpm->rpw, "ciphersuite","NULL");
    }else{ 
        if(reportevent.report_tls_findings.cipher_analytic_results.ciphers_count>0){ 
            report_writer_write_value_str(rpm->rpw, "ciphersuite", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[0].cipher_name);
        }else{
            report_writer_write_value_str(rpm->rpw, "ciphersuite","ERROR");
        }  
    } 

    report_writer_enter_array(rpm->rpw, "ciphers");
    
    for(int i=0;i<reportevent.report_tls_findings.cipher_analytic_results.ciphers_count;i++){
        char cipherid_str[20];
        sprintf(cipherid_str,"0x%02x, 0x%02x", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].cipher_id[0], reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].cipher_id[1]); 
        report_writer_write_value_str(rpm->rpw, "cipher-id", cipherid_str);
        report_writer_write_value_str(rpm->rpw, "cipher-name", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].cipher_name);
        report_writer_write_value_int(rpm->rpw, "recommended", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].recommended);
        report_writer_write_value_str(rpm->rpw, "tls-type", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].type==0x01?"offer":"select");
        report_writer_write_value_int(rpm->rpw, "security", reportevent.report_tls_findings.cipher_analytic_results.individual_ciphers[i].security);
    } 
    
    report_writer_leave_array(rpm->rpw);
    
    report_writer_leave_object(rpm->rpw);
    return;
}


int reportmanager_deinit(report_manager *rpm){
    report_writer_leave_array(rpm->rpw);
    time_t clk = time(NULL);
    char timebuffer[50];
     strcpy(timebuffer, ctime(&clk));
     timebuffer[strlen(timebuffer)-1]=0;
    report_writer_write_value_str(rpm->rpw, "stop-timestamp",timebuffer);
    report_writer_write_value_int(rpm->rpw, "nw_total_packet_count", rpm->total_packet_count);
    report_writer_write_value_int(rpm->rpw, "nw_identified_packet_count", rpm->identified_packet_count);
    report_writer_leave_object(rpm->rpw);
    //report_writer_close(rpm->rpw);
    free(rpm);

    return 0;
} 
