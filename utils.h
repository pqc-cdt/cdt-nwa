#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdarg.h>

/**
 * Formats a command string, similar to printf, and runs it as an external process.
 * The stdout of the external process is returned.
 * @param format_string The format command string
 * @param ... All additional parameters that will be combined with the format string
 * @return The stdout after running the format string as external process.
 */
char *run_external_command(const char *format_string, ...);


#endif