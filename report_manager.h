#ifndef REPORT_MANAGER_H
#define REPORT_MANAGER_H

#include "report_event.h"
#include "report_writer.h"

typedef struct{
    report_writer *rpw;
    int total_packet_count;
    int identified_packet_count;
    int write_after;
} report_manager;


//reportdata are managed here before they are send to the report-writer.
//E.g. to minimize disk-io if required.

//write_after:1 immediately, +2....int_max: reserve a buffer and write when buffer ful.
//-2...int_min: write at shutdown and reserve initial buffer of given absolute value.
// the buffer is expanded by EXPANSION_RATE percent when ful.
report_manager* reportmanager_init(report_writer *rpw, int write_after);//TODO: write_after implementation
//TODO: Abstraction between reportmanager and reportwriter

void reportmanager_add_unidentified(report_manager *rpm);
void reportmanager_add_entry(report_manager *rpm, report_event reportevent);

int reportmanager_write_entries(report_manager *rpm);
int reportmanager_write_stats(report_manager *rpm);

int reportmanager_deinit(report_manager *rpm);

#endif