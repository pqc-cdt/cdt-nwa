#include <stdlib.h>
#include <stdio.h>
#include "lsof_parser.h"

int resolve_origin_process(uint16_t src_port, char *dest_buffer, int dest_buffer_size) {

    int resultlength=0;
    char command_buffer[80];
    char result_buffer[200];
    FILE *lsof_stdstream;
    if (src_port <= 0 || src_port > 65535)
        return -1;

    sprintf(command_buffer, "lsof -n -i :%d | awk 'FNR == 1 {next} {print $1}'", src_port);
    lsof_stdstream = popen(command_buffer, "r");
    fgets(result_buffer, 199, lsof_stdstream);
    result_buffer[199]=0;
    pclose(lsof_stdstream);
    printf("result: %s\n ", result_buffer);
    resultlength=snprintf(dest_buffer, dest_buffer_size, "%s", result_buffer);
    if(dest_buffer[resultlength-1]=='\n'){
        dest_buffer[resultlength-1]=0;
    } 
    return 0;
}