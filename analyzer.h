#ifndef ANALYZER_H
#define ANALYZER_H

#include "analyzer_result.h"

/**
 * Evaluate if data contains TLS header
 * @param data Binary data after TCP header
 * @param data_length length of data
 * @return returns 1 in case of tls header, 0 in case of no tls header, -1 in case of delivered data too short for evaluation
 */
int is_tls_header(unsigned char *data, int data_length);


/**
 * TLS handshake analysis. Analyze ciphersuite proposed and selected in handshake
 * @param package Data after tcp header
 * @param buffer_length length of delivered package data
 * @param original_package_length length of original received package
 * @return findigs as struct
 */
analyzer_result analyze_package(unsigned char *package, int buffer_length, int original_package_length); // two length, in case not all data where captured.

#endif