
all:
	#gcc -Wall -Wextra -g3 main.c device_manager.c utils.c analyzer.c lsof_parser.c report_manager.c report_writer_json.c -o cdt-na -I/usr/include/pcap -lpcap
	clang -Wall -Wextra -g3 main.c device_manager.c utils.c analyzer.c lsof_parser.c report_manager.c report_writer_json.c -o cdt-na -I/usr/include/pcap -lpcap

clean:
	rm *.o
	rm cdt-na
