#include <stdlib.h>
#include <string.h>
#include "cipher_suite_ids.h"
#include "analyzer_result.h"


cipher_suite_entry cipher_id_table[CIPHER_SUITES_ARRAY_LENGTH] = CIPHER_SUITES_ARRAY;
cipher_suite_classification_entry cipher_id_classification_table[CIPHER_SUITE_CLASSIFICATIONS_ARRAY_LENGTH] = CIPHER_SUITE_CLASSIFICATIONS_ARRAY;


int is_tls_header(unsigned char *data, int data_length){

    if(data_length<11){ 
        return -1;
    }
    
    if(data[0]==0x16 && data[1]==0x03 && (data[2]==0x01|| data[2]==0x03|| data[2]==0x04 ) && (data[5]==0x01|| data[5]==0x02 ))
    {   
        return 1;
    } 
    
    return 0;

} 
//Messagetype: 0x01: Client-Hello, 0x02: Server-Hello
ciphersuites_analytic_results analyze_ciphersuites(unsigned char* ciphersuites_identifiers, int length_in_byte, int messagetype){
    ciphersuites_analytic_results car;

    car.ciphers_count=0;
    car.individual_ciphers=NULL;
    car.offered_highest_index=-1;
    car.offered_lowest_index=-1;
    car.selected_index=-1;

    if(length_in_byte%2==0){
        car.ciphers_count=length_in_byte/2;
        
        car.individual_ciphers=(ciphersuites_analytic_result*)malloc(sizeof(ciphersuites_analytic_result)*car.ciphers_count);
        
        if(car.individual_ciphers==NULL){
            printf("Error: malloc failed");
            return car;
        } 
        for(int i=0; i<car.ciphers_count;i++){
            
            int found_index=-1;            
            for(int j=0; j<CIPHER_SUITES_ARRAY_LENGTH;j++){
                if(ciphersuites_identifiers[2*i]==cipher_id_table[j].id_0 && ciphersuites_identifiers[(2*i)+1]==cipher_id_table[j].id_1 ){
                    
                    found_index=j;
                    break;
                } 
            } 
            if(found_index!=-1)
            {
                car.individual_ciphers[i].cipher_id[0] =cipher_id_table[found_index].id_0;
                car.individual_ciphers[i].cipher_id[1] =cipher_id_table[found_index].id_1;
                car.individual_ciphers[i].recommended = cipher_id_table[found_index].recommended;
                strcpy(car.individual_ciphers[i].cipher_name, cipher_id_table[found_index].name );
                if(messagetype==0x01){
                    car.individual_ciphers[i].type=0;
                } else{
                    car.individual_ciphers[i].type=1;
                } 
            } else 
            {
                car.individual_ciphers[i].cipher_id[0] =ciphersuites_identifiers[2*i];
                car.individual_ciphers[i].cipher_id[1] =ciphersuites_identifiers[(2*i)+1];
                car.individual_ciphers[i].recommended = 0;
                strcpy(car.individual_ciphers[i].cipher_name, CIPHER_NAME_UNKNOWN );
                if(messagetype==0x01){
                    car.individual_ciphers[i].type=0;
                } else{
                    car.individual_ciphers[i].type=1;
                }
            } 

            //search pq-security information
            found_index=-1;            
            for(int j=0; j<CIPHER_SUITE_CLASSIFICATIONS_ARRAY_LENGTH;j++){
                if(ciphersuites_identifiers[2*i]==cipher_id_classification_table[j].id_0 && ciphersuites_identifiers[(2*i)+1]==cipher_id_classification_table[j].id_1 ){
                    
                    found_index=j;
                    break;
                } 
            } 
            if(found_index!=-1)
            {
                if(cipher_id_classification_table[found_index].is_insecure_or_broken==1){
                    car.individual_ciphers[i].security= -2;    
                } else if (cipher_id_classification_table[found_index].is_pqc_secure==1){
                    car.individual_ciphers[i].security= +2;
                } else{
                    car.individual_ciphers[i].security= 0;
                } 
                
            } else
            {
                car.individual_ciphers[i].security= 0;
            }  

        } 

        if(messagetype==1){
            int max_security, min_security;
            int index_max=0, index_min=0;
            for(int i=0; i<car.ciphers_count;i++){
                if(i==0){
                    max_security=car.individual_ciphers[0].security;
                    min_security=max_security;
                }else{
                    if(car.individual_ciphers[i].security<min_security){
                        min_security=car.individual_ciphers[i].security;
                        index_min=i;
                    } 
                    if(car.individual_ciphers[i].security>max_security){
                        max_security=car.individual_ciphers[i].security;
                        index_max=i;
                    } 
                }  
                car.offered_highest_index=index_max;
                car.offered_lowest_index=index_min;
            } 
        } else {
            
            car.selected_index=0;
        } 

    } else{
        printf("Error: Ciphersuites Length invalid\n");
    } 

    
    return car;
} 


analyzer_result analyze_package(unsigned char *package, int buffer_length, int original_package_length) 
{
    int buffer_cursor;
    int ciphersuites_length;
    int compression_method_length=0;
    int messagetype;
    int message_length_after_tls_version=0;
    int extension_length;
    int extensions_cursor;
    analyzer_result ar;
    ciphersuites_analytic_results cars;
    record_hello_simpel *overlay=0;
    printf("Analyzer: begin\n");
    if(buffer_length<43){
        ar.tls_request_type=-1;
        ar.tls_version=-1;
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Analyzer: error: (1) too short\n");
        return ar;
    } 
    overlay=(record_hello_simpel*) package;
    if(overlay->record_header[0]!=0x16) //handshake record (is equal for server and client)
    {
        ar.tls_request_type=-1;
        ar.tls_version=-1;
        ar.analyzer_errors=ANALYZER_ERROR_NO_HANDSHAKE_RECORD;
        printf("Analyzer: error: (1) no handshake\n");
        return ar;
    } 
    if(overlay->record_header[1]!=0x03||  (overlay->record_header[2]!=0x03 &&overlay->record_header[2]!=0x01&&overlay->record_header[2]!=0x04 )){
        ar.tls_request_type=-1;
        ar.tls_version=-1;
        ar.analyzer_errors=ANALYZER_ERROR_TLS_VERSION_INVALID;
        printf("Analyzer: error: (1) tls version invalid\n");
        return ar;
    }
    messagetype=overlay->handshake_header[0]; 
    ar.tls_request_type=messagetype; //02: server hello, 01: clienthello.
    ar.tls_version=(((unsigned short)(overlay->record_header[1])<<8)&0xff00)|(((unsigned short)(overlay->record_header[2]))&0x00ff);
    message_length_after_tls_version=(((unsigned short)(overlay->record_header[3])<<8)&0xff00)|(((unsigned short)(overlay->record_header[4]))&0x00ff);
    //check length
    if((message_length_after_tls_version+5)>buffer_length){
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Analyzer: warning: (2) too short; buffer: %d, required: %d\n", buffer_length, (message_length_after_tls_version+5));
        //return ar;
    } 
    
    buffer_cursor=package[43]+44;


    printf("session-id length fileds content: %d\n",package[43]);
    printf("Cursor_position for ciphersuite-length: %d\n", buffer_cursor);

    if(buffer_cursor>buffer_length){
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Analyzer: error: (3) too short; buffer: %d, required: %d\n", buffer_length, buffer_cursor+1);
        return ar;
    }

    if(messagetype==0x01){
    ciphersuites_length=(((unsigned short)(package[buffer_cursor])<<8)&0xff00)|(((unsigned short)(package[buffer_cursor+1]))&0x00ff);
    //make validity check of length / endpos.
    if(buffer_cursor+2+ciphersuites_length>=buffer_length){
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Analyzer: error: (3) too short\n");
        return ar;
    }
        cars=analyze_ciphersuites(&package[buffer_cursor+2], ciphersuites_length, messagetype );
        buffer_cursor=buffer_cursor+2+ciphersuites_length;
        compression_method_length=package[buffer_cursor];
        buffer_cursor=buffer_cursor+compression_method_length+1; 
    }  else{
        cars=analyze_ciphersuites(&package[buffer_cursor], 2, messagetype );
        buffer_cursor=buffer_cursor+3;
    } 
    ar.cipher_analytic_results=cars;
    
    printf("compression-method-length:%d\n", compression_method_length);
    //cursor is now at first byte of extensions length
    if(buffer_cursor+1>=buffer_length){
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Analyzer: error: (4) too short\n");
        return ar;
    }
    extension_length=(((unsigned short)(package[buffer_cursor])<<8)&0xff00)|(((unsigned short)(package[buffer_cursor+1]))&0x00ff);
    if(buffer_cursor+1+extension_length>=buffer_length){
        ar.analyzer_errors=ANALYZER_ERROR_TOO_SHORT;
        printf("Extensions-length: %d\n", extension_length);
        printf("Analyzer: error: (5) too short\n");
        return ar;
    }
    // extensions
    buffer_cursor=buffer_cursor+2;
    extensions_cursor=0;

    printf("Analyzer: extensions begin. Length: %d\n", extension_length);
    while(extensions_cursor<extension_length)
    {
        unsigned char extension_id[2];
        int this_extension_length; 
        int this_extension_cursor=0;
        extension_id[0]= package[buffer_cursor+extensions_cursor];
        extension_id[1]= package[buffer_cursor+extensions_cursor+1];
        this_extension_length=(((unsigned short)(package[buffer_cursor+extensions_cursor+2])<<8)&0xff00)|(((unsigned short)(package[buffer_cursor+extensions_cursor+3]))&0x00ff);
        
        if(extension_id[0]==0x00 ){
            switch(extension_id[1]){
                case 0x0A: //supported groups extension
                printf("TODO: Supported Groups extension present\n ");
                //TODO: To be implemented.
                break;
                case 0x0D: //Signature Algorithms
                printf("TODO: Signature Algorithms extension present\n ");
                break;
                case 0x2B: //Suported Versions
                printf("TODO: Supported Versions extension present\n ");
                break;
                case 0x33: //Key Share
                printf("TODO: Key Share extension present\n ");
                break;

                default:
                printf("TODO: Unhandled extension present\n ");
                break;
            } 
        } 
        
        extensions_cursor=extensions_cursor+this_extension_length+4;
    } 

    printf("Analyzer: done\n");
    return ar;

}
