#include <stdlib.h>
#include <stdio.h>
#include "report_writer.h"


report_writer* report_writer_init(){
    report_writer* rpw;
    rpw=(report_writer*)calloc(1, sizeof(report_writer));
    rpw->report_file=NULL;
    rpw->separator_required=0;
    return rpw;
}

void report_writer_deinit(report_writer* rpw){
    report_writer_close_file(rpw);
    free(rpw);
} 


int report_writer_open_file(report_writer* rpw, char *path){
    report_writer_close_file(rpw);
    rpw->report_file=fopen(path, "w");
    if(rpw->report_file==NULL){
        return -1;
    } 
    rpw->separator_required=0;

    return 0;
}


int report_writer_close_file(report_writer* rpw)
{
    if(rpw->report_file!=NULL){
        fflush(rpw->report_file);
        fclose(rpw->report_file);
        rpw->report_file=NULL;
    } 
    return 0;
}


int report_writer_enter_array(report_writer* rpw, char *name)//name can be NULL if we are in an array
{
    if(rpw->report_file==NULL){
        return -1;
    } 
    if(rpw->separator_required!=0){
        fprintf(rpw->report_file, ", ");
    }
    if(name!=NULL){
        fprintf(rpw->report_file, "\"%s\" : ", name);
    } 
    fprintf(rpw->report_file, "[ \n");
    rpw->separator_required=0;
    return 0;
}


int report_writer_leave_array(report_writer* rpw)//place closing array bracket
{
    if(rpw->report_file==NULL){
        return -1;
    } 
    fprintf(rpw->report_file, " ] \n");
    rpw->separator_required=1;

    return 0;
} 


int report_writer_enter_object(report_writer* rpw, char *name)//name can be NULL if we are in an array
{
if(rpw->report_file==NULL){
        return -1;
    } 
    if(rpw->separator_required!=0){
        fprintf(rpw->report_file, ", ");
    }
    if(name!=NULL){
        fprintf(rpw->report_file, "\"%s\" : ", name);
    } 
    fprintf(rpw->report_file, "{ \n");
    rpw->separator_required=0;
    return 0;
}


int report_writer_leave_object(report_writer* rpw)//place closing object bracket
{
if(rpw->report_file==NULL){
        return -1;
    } 
    fprintf(rpw->report_file, " } \n");
    rpw->separator_required=1;

    return 0;
}


int report_writer_write_value_int(report_writer* rpw, char *name, int value)//name can be NULL in case of array
{
    if(rpw->report_file==NULL){
        return -1;
    } 
    if(rpw->separator_required!=0){
        fprintf(rpw->report_file, ", ");
    }
    if(name!=NULL){
        fprintf(rpw->report_file, "\"%s\" : ", name);
    } 
    fprintf(rpw->report_file, "%d \n", value);
    rpw->separator_required=1;
    return 0;
}


int report_writer_write_value_str(report_writer* rpw, char *name, char *value)//name can be NULL in case of array
{
    if(rpw->report_file==NULL){
        return -1;
    } 
    if(rpw->separator_required!=0){
        fprintf(rpw->report_file, ", ");
    }
    if(name!=NULL){
        fprintf(rpw->report_file, "\"%s\" : ", name);
    } 
    fprintf(rpw->report_file, "\"%s\" \n", value);
    rpw->separator_required=1;
    return 0;
}

