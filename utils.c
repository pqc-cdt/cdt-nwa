#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/resource.h>


char *run_external_command(const char *format_string, ...) {

    if (!format_string) return NULL;

    // Combine command parts into a buffer
    va_list args;
    va_start(args, format_string);
    char command_buffer[1024];
    vsnprintf(command_buffer, 1024, format_string, args);
    va_end(args);

    // Execute the command string
    FILE *fp = popen(command_buffer, "r");
    if (fp == 0) {
        return NULL;
    }

    size_t bytes_read = 0;
    size_t buffer_size = 0;
    size_t buffer_offset = 0;
    size_t total_bytes_read = 0;

    char *buffer = NULL;

    while (1) {
        // Increase buffer size
        buffer = (char *) realloc((void *) buffer, buffer_size + 64);
        if (!buffer) return NULL; // ToDo: Handle realloc failure properly

        buffer_size += 64;

        // Read the bytes
        bytes_read = fread(buffer + buffer_offset, sizeof(char), buffer_size - buffer_offset, fp);
        total_bytes_read += bytes_read;
        if (bytes_read == 0) break;

        // Increase offset
        buffer_offset += 64;
    }

    // ToDo: This might require a realloc
    buffer[total_bytes_read] = '\0';

    pclose(fp);
    return buffer;
}
