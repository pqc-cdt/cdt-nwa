#ifndef ANALYZER_RESULT_H
#define ANALYZER_RESULT_H


//Definitions of analyzer errors
#define ANALYZER_ERROR_NO_ERROR 0
#define ANALYZER_ERROR_TOO_SHORT -1
#define ANALYZER_ERROR_NO_HANDSHAKE_RECORD -2
#define ANALYZER_ERROR_TLS_VERSION_INVALID -3



typedef struct {
unsigned char record_header[5];//type, version(2), bytes of following message(2)
unsigned char handshake_header[4];//client/serverhello(1) length of following message (3)
unsigned char client_version[2]; //0x0303 
unsigned char client_random[32];
//unsigned char session_id;//starts with the length of session id.
//unsigned char cipher_suites[32]; //each cs has two bytes which tell the length
//unsigned char compression_methods[2];
//unsigned char extensions_length[2] ;
//extensions will now follow with 2 bytes extension dentifier plus two bytes extension-content-length

} record_hello_simpel;//43 bytes

typedef struct {
    int length;
    unsigned char* content;

} extension_frame;//used to comfortably point to an extension consisting of length and data

//Primary variable length fields to handle
// session id
// ciphersuites
// compression methods
// static fields after compressionme. to handle
// extensions-length


//extensions to handle:
// Extension - Supported Groups, 00 0a, contains a list of elliptic curves
// Extension - EC Point Formats, 00 0b, contains info about compression??
// Extension - Signature Algorithms, 00 0d, contains list of supported cyphers for signatures
// Extension - Renegotiation Info, ff 01, contains extension length and one byte of use data, which is 00 for the clienthello

typedef struct {
    int security; // -2 ...2; -2: unknown, -1: not applicable, 0: insecure, 1: classically secure, 2: pq-secure
    int recommended; // 0, 1
    int type; //0: offer, 1: select
    unsigned char cipher_id[2];
    char cipher_name[100];//longest in list is 45 characters ... 100 to be future proof

} ciphersuites_analytic_result;

typedef struct {
    ciphersuites_analytic_result *individual_ciphers;
    int ciphers_count;
    //int selected_security;//-1 not applicable, 0: insecure, 1: classical_secure, 2: pq secure
    int selected_index;//-1 not applicable
    int offered_lowest_index; //lowest offered security; -1 not applicable
    int offered_highest_index;// highest security offer; -1 not applicable
} ciphersuites_analytic_results;



typedef struct{
    int tls_version;//negative if not tls;
    int tls_request_type;//clienthello, serverhello
    //void *proposed_cypher_list;
    //int proposed_cypher_list_length;
    //void *accepted_cypher_list;
    //int accepted_cypher_list_length;
    //int session_id_eval;//0 real, 1 fake

    ciphersuites_analytic_results cipher_analytic_results;
    int analyzer_errors;//feedback, e.g. delivered package content too short.

} analyzer_result;

#endif