#ifndef DEVICE_MANAGER_H
#define DEVICE_MANAGER_H
#include "report_manager.h"
typedef struct device_manager {
    pcap_if_t *devices;
    pcap_t *handle;
    int device_count;
    char dev_errbuf[PCAP_ERRBUF_SIZE];
    report_manager *rpm;
} device_manager;


device_manager *device_manager_init(report_manager* rpm);

void device_manager_list_devices(device_manager *manager);

void device_manager_start_capture(device_manager *manager);

void device_manager_handle_packet(u_char *user_data, const struct pcap_pkthdr *pkthdr, const u_char *packet);

void device_manager_stop_capture(device_manager *manager);

void device_manager_free(device_manager *manager);

#endif