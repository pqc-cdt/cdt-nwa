# cdt-nwa

## Introduction

Quantum computers are a rapidly advancing area of research with the potential to revolutionize computing as we know it. 
However, their ability to quickly solve certain types of mathematical problems could also pose a significant threat to current cryptographic systems.
The Crypto-Detection-Tool project aims to provide a comprehensive suite of tools designed to assist system administrators in evaluating the resilience of the cryptographic components installed on a Linux system against potential threats posed by post-quantum computers

## Summary

This tool, cdt-nwa, is part of the crypto-detection-tool project.
It will capture network traffic and filter for tls handshakes, which are then analyzed for the selected ciphersuite. 
The ciphersuite is written together with the remote ip address as well as the local application handling the connection into a json based report.


## Features

Currently, cdt-nwa provides the following features:

- identify TLS handshakes
- analyze ciphersuite selected during handshake
- identify application name of local application owning the socket

## How does it work?

1. The interface is captured using pcap
2. Incomming packets are filtered for beeing IPv4 and tcp traffic.
3. The TLS record header is evaluated to filter for TLS handshakes.
4. The TLS handshakeheader is evaluated to further correctly process Client Hello and Server Hello messages.
5. The ciphersuite-IDs are resulved to Names through a lookup-table.
6. The application name is resolved through the local port number using lsof.
7. The entry is written to the report file.

### Work in Progress
- Switch from PCAP to eBPF

## Future work

- TLS extensions evaluation (e.g. supported groups)
- TLS Client authentication
- Findings in line with CBOM (https://github.com/IBM/CBOM)
- Extended Protocol-support
- Clientauthentication analysis

The list is endless...


## Dependencies:
- pcap

