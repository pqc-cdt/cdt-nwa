#ifndef REPORT_WRITER_H
#define REPORT_WRITER_H
#include <stdio.h>
typedef struct{
    FILE* report_file;
    int separator_required;
} report_writer;

report_writer* report_writer_init();
void report_writer_deinit(report_writer* rpw);

int report_writer_open_file(report_writer* rpw, char *path);
int report_writer_close_file(report_writer* rpw);

int report_writer_enter_array(report_writer* rpw, char *name);//name can be NULL if we are in an array
int report_writer_leave_array(report_writer* rpw);//place closing array bracket


int report_writer_enter_object(report_writer* rpw, char *name);//name can be NULL if we are in an array
int report_writer_leave_object(report_writer* rpw);//place closing object bracket

int report_writer_write_value_int(report_writer* rpw, char *name, int value);//name can be NULL in case of array
int report_writer_write_value_str(report_writer* rpw, char *name, char *value);//name can be NULL in case of array

#endif