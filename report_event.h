#ifndef REPORT_EVENT_H
#define REPORT_EVENT_H

#include "analyzer_result.h"

union ip_mem
{
    unsigned char as_u8[16];//0...3 is ipv4
    unsigned short as_u16[8];
    unsigned int as_u32[4];//as_u32[0] is the ip v4 addr.
};

typedef struct {
    union ip_mem src_ip;
    union ip_mem dst_ip;
    int src_port;
    int dst_port;
    int ip_ver;//4, 6 
} report_sockdef;

typedef struct {
    report_sockdef report_socket;
    analyzer_result report_tls_findings;
    int package_direction; //recv / send.
    int pid;
    int gid;
    char appname[16]; 
} report_event;

#endif