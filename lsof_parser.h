#ifndef LSOF_PARSER_H
#define LSOF_PARSER_H

#include <stdint.h>

/**
 * Resolve process name by tcp port number
 * @param src_port tcp source port
 * @param dest_buffer pointer to buffer to return process name
 * @param dest_buffer_size size of buffer to return process name
 * @return 0 on success, -1 on error
 */
int resolve_origin_process(uint16_t src_port, char *dest_buffer, int dest_buffer_size);


#endif